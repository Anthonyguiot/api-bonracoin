<?php
require_once "../vendor/autoload.php";


\app\App::EloConfigure('../conf/config.ini');

$app = new \Slim\Slim();
session_start();

$app->get('/', function() use ($app) {
	$app->redirect($app->urlFor('admin'));
});

$app->get('/admin', function() use ($app) {
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),
	'templates.path' =>'../src/racoin/backend/templates']); 
	$app->render( 'admin.html.twig',
	array('id' => 'Identifiant:', 
	'mdp' =>  'Mot de passe:','title'=> 'Authentification:' ));
})->name("admin");

$app->post('/login', function () use ($app) {
	$id   = $app->request->post('id');
	$mdp  = $app->request->post('password');
	$c = new \racoin\backend\controller\AdminController();
	$c->requestcredentials($id,$mdp);
	$app = new \Slim\Slim;
});
$app->get('/logout', function () use ($app) {
	echo "logged off";
	session_destroy();
});

$app->get('/authfail', function () use ($app) {
	echo "Login ou mot de passe incorrect";
})->name("fail");

$app->get('/annonceNonValide', function() use ($app) {
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),
	'templates.path' =>'../src/racoin/backend/templates']); 
	if (isset($_SESSION['user'])) {
	$c = new \racoin\backend\controller\AnnonceController();
	$res = $c->AnnoncesNonValide();

	// Get request object
	$req = $app->request;

	//Get root URI
	$rootUri = $req->getRootUri();

	$app->render( 'annonceNonValide.html.twig',
	array('annonces' => $res, 'rootUri' => $rootUri));
	}
	else{
		echo "Veuillez vous authentifier";
	}

})->name('annoncenonvalide');


$app->get('/annonce/:id', function($id) use ($app) {
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),
	'templates.path' =>'../src/racoin/backend/templates']); 
	if (isset($_SESSION['user'])) {
	$c = new \racoin\backend\controller\AnnonceController();
	$res = $c->getAnnoncebyId($id);
	$annonceur = $c->AnnoncesCoordonneesAnnonceur($id);

	// Get request object
	$req = $app->request;
	//Get root URI
	$rootUri = $req->getRootUri();

	$app->render( 'annonce.html.twig',
	array('id' => $res['id'],'titre' => $res['titre'],
		'descriptif' => $res['descriptif'],'prix' => $res['prix'],
		'dep_id' => $res['dep_id'],'nom_a' => $annonceur[0]['nom_a'],
		'prenom_a' => $annonceur[0]['prenom_a'],'mail_a' => $annonceur[0]['mail_a'],
		'rootUri' => $rootUri));
	
	}
	else{
		echo "Veuillez vous authentifier";
	}
});

$app->get('/annonceEtat/:id/:etat', function($id,$etat) use ($app) {
	if (isset($_SESSION['user'])) {
	// Get request object
	$req = $app->request;
	//Get root URI
	$rootUri = $req->getRootUri();

	$c = new \racoin\backend\controller\AnnonceController();
	$res = $c->etatAnnonce($id,$etat,$rootUri);
	}
	else{
		echo "Veuillez vous authentifier";
	}
});



$app->run();

/*
$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),
	'templates.path' =>'../src/racoin/backend/templates']); 

$app->render( 'test.html.twig',
	array('title' => 'Ah, quel BEAU TITRE', 
	'message' =>  'Oh, le beau Message' ));

	*/