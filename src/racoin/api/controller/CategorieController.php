<?php
namespace racoin\api\controller ;


class CategorieController
{
	
	public function typeCategorie($id){
			$app = \Slim\Slim::getInstance();
		$categorie = \racoin\common\model\Categorie::where('id','=',$id)->get();
		if (count($categorie) > 0) {
		$cat = $categorie->toArray();
		$res=[
		'categorie'=> $cat,
		'links'=>[ 
			'categories' => ['href'=> $app->urlFor('annBycat', ['id'=>$id])]
			]
		];
		echo json_encode($res, JSON_FORCE_OBJECT);
		$app->response->headers->set('Content-Type', 'application/json');
	}
		else {
		echo "Mauvais parametre";
		$app->response->setStatus(400);
		}
}

	public function AllCategorie(){
		$app = \Slim\Slim::getInstance();
		$all_categorie = \racoin\common\model\Categorie::all();
		$cat = $all_categorie->toArray();
		for($i=0;$i<count($cat);$i++){
			$res[]=[
			'annonce'=> $cat[$i],
			'links'=>[ 
				'annonces' => ['href'=> $app->urlFor('catByid', ['id'=>$cat[$i]['id']])]
				]
			];
		}
		echo json_encode($res, JSON_FORCE_OBJECT);
		$app->response->headers->set('Content-Type', 'application/json');
	}

	public function CategorieOfAnnonce($id){
		$app = \Slim\Slim::getInstance();

		$annonce = \racoin\common\model\Annonce::find($id); 
		$categorie = \racoin\common\model\Categorie::select('id', 'libelle')->where('id', '=', $annonce["cat_id"])->first() ;
		if (is_object($annonce)) {
		$cat = $categorie->toArray();
		$res=[
		'categorie'=> $cat,
		'links'=>[ 
			'categorie' => ['href'=> $app->urlFor('catByid', ['id'=>$id])]
			]
		];
		echo json_encode($res, JSON_FORCE_OBJECT);
		$app->response->headers->set('Content-Type', 'application/json');
	}
	else {
		echo "Id d'annonce inexistante";
		$app->response->setStatus(400);
	}
	}


}