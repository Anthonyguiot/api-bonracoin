<?php
namespace racoin\api\controller ;

class AnnonceController
{


public function getListAnnonces($offset, $take, $min, $max){
		$app = \Slim\Slim::getInstance();
			if($take == ''){
				$take = 5;
			}

			$annonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','cat_id','dep_id')
			->where('status','=','1')
			->orderBy('updated_at','desc');
			
			$annonceForLast = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','cat_id','dep_id')
			->where('status','=','1')
			->orderBy('updated_at','desc');
			
			if (!empty($take)) {
				$annonce = $annonce->take($take);
			}

			if (!empty($offset)){
				$annonce = $annonce->skip($offset);
			}

			if(!empty($min)) {
				$annonce = $annonce->where('prix','>',$min);
				$annonceForLast = $annonceForLast->where('prix','>',$min);
			}	

			if(!empty($max) ) {
				$annonce = $annonce->where('prix','<',$max);
				$annonceForLast = $annonceForLast->where('prix','<',$max);

			}

			$annonce = $annonce->get();
			$annonceForLast = $annonceForLast->get();
			

			$ann = $annonce->toArray();
			$annonces = array();
			foreach ($ann as $key => $value) {
				$tab=[
				'annonce'=> $value,
				'links'=>['self'=>['href'=> $app->urlFor('list2ann', ['id'=>$value['id']])]]
				];
				$annonces[]=$tab;
			}
			$res["annonces"]=$annonces;
			
			$offsetprev = $offset-$take;
			if ($offsetprev > 0){
				$prev=["href"=>$app->urlFor("listann")];
				if (!empty($min)){
					$prev["href"] = $prev["href"]."?min=$min";
				}

				if (!empty($min)){
					$prev["href"] = $prev["href"]."?min=$min&max=$max&take=$take&offset=$offsetprev";
				}

				$prev["href"] = $prev["href"]."?min=$min&max=$max&take=$take&offset=$offsetprev";
				$res["links"]["prev"]=$prev;
			}

			$offsetnext = $take+$offset;
			$next=["href"=>$app->urlFor("listann")];
			$next["href"] = $next["href"]."?min=$min&max=$max&take=$take&offset=$offsetnext";
			$res["links"]["next"]=$next;
			
			$first=["href"=>$app->urlFor("listann")];
			$first["href"] = $first["href"]."?min=$min&max=$max&take=$take";
			$res["links"]["first"]=$first;
			
			$offsetlast = ceil( $annonceForLast->count() / $take );
			$last=["href"=>$app->urlFor("listann")];
			$last["href"] = $last["href"]."?min=$min&max=$max&take=$take&offset=$offsetlast";
			$res["links"]["last"]=$last;

			if ($min>$max) {
				echo "Prix min superieur au prix max";
				$app->response->setStatus(400);
			}	else{
				echo json_encode($res);
				$app->response->headers->set('Content-Type', 'application/json');
				$app->response->setStatus(201);
			}

}





public function getAnnoncebyId($id){
		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','cat_id','dep_id')->where('id','=',$id)->first();
			if (!is_object($annonce)) {
				echo "Mauvais parametre";
				$app->response->setStatus(400);
			}
			elseif (is_object($annonce)) {
			$ann = $annonce->toArray();
			$res=[
			'annonce'=> $ann,
			'links'=>[ 
				'categorie' => ['href'=> $app->urlFor('ann2cat', ['id'=>$id])],
				'annonceur' => ['href'=> $app->urlFor('ann2annonceurs', ['id'=>$annonce->id])],
				'department' => ['href'=> $app->urlFor('ann2dep', ['id'=>$annonce->code_postal])],
				]
			];
			echo json_encode($res, JSON_FORCE_OBJECT);
			$app->response->setStatus(201);
			$app->response->headers->set('Content-Type', 'application/json');
			}

		//$annonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','nom_a','mail_a','tel_a','cat_id','dep_id')->where('id','=',$id)->first();
			// $tmpl = $twig->loadTemplate('test.html.twig');
			// $tmpl->display( array ( 
			// 	'title' => 'Ah, quel BEAU TITRE', 
			// 	'message' =>  'Oh, le beau Message'
			// )) ;
	}


public function AnnoncesByCategorie($id,$min,$max){
		$app = \Slim\Slim::getInstance();
		if ($min>$max) {
			echo "Prix min superieur au prix max";
			$app->response->setStatus(400);
		}
		else{
		if(empty($min)&&empty($max)) {
			$annonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','nom_a','mail_a','tel_a','cat_id','dep_id')->where('cat_id','=',$id)->get();
		}
		else{
			
			$annonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','nom_a','mail_a','tel_a','cat_id','dep_id')->where('cat_id','=',$id)->whereBetween('prix', [$min,$max])->get();
		}
		if (count($annonce) > 0) {
		$ann = $annonce->toArray();
		$res = array();
		foreach ($ann as $key => $value) {
			$tab=[
			'annonce'=> $value,
			'links'=>['self'=>['href'=> $app->urlFor('list2ann', ['id'=>$value['id']])]]
			];
			$res[]=$tab;
		}
		echo json_encode($res, JSON_FORCE_OBJECT);
		$app->response->headers->set('Content-Type', 'application/json');
	}
	else{
		echo "Mauvais parametre";
		$app->response->setStatus(400);

	}
	}
}

public function AnnoncesNonValide(){

		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::select('id','titre','prix','nom_a','created_at')->where('status','=',0)->orderBy('created_at', 'ASC')->get();
		if (!is_object($annonce)) {
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
		else{
			$ann = $annonce->toArray();
			for($i=0;$i<count($ann);$i++){
				$res[]=[
				'annonce'=> $ann[$i],
				'links'=>[ 
					'annonces' => ['href'=> $app->urlFor('list2ann', ['id'=>$ann[$i]['id']])],
					]
				];
			}
			echo json_encode($res, JSON_FORCE_OBJECT);
			$app->response->setStatus(201);
			$app->response->headers->set('Content-Type', 'application/json');
		}

	}

public function AnnoncesSuppr($id,$mdp){

		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::find($id);
		$hash = $annonce['passwd'];
		if (!is_object($annonce)) {
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
		else{
			if(password_verify($mdp, $hash))
			{
				$annonce->status = '10';
				$annonce->save();
				echo("Annonce ".$id." supprimée");
				$app->response->setStatus(201);
			}
			else{
				echo'Mauvais mot de passe';
				$app->response->setStatus(400);
			}
		}
		$app->response->headers->set('Content-Type', 'application/json');

	}

public function AnnoncesValider($id){

		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::find($id); 
		if (is_object($annonce)) {
		$annonce->status = '1';
		$annonce->save();
		echo("Annonce validée");
		$app->response->setStatus(201);
		$app->response->headers->set('Content-Type', 'application/json');
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
	}

public function AnnoncesCoordonneesAnnonceur($id){
		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::select('nom_a','prenom_a','mail_a','tel_a')->where('id','=',$id)->get();
		if (count($annonce) > 0) {
		$ann = $annonce->toArray();
		$res=[
		'annonce'=> $ann,
		];
		echo json_encode($res, JSON_FORCE_OBJECT);
		$app->response->setStatus(201);
		$app->response->headers->set('Content-Type', 'application/json');
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
		 
}

public function ajoutAnnonce($titre,$descriptif,$code_postal,$prix,$nom_a,$passwd,$cat_id,$dep_id){
		$app = \Slim\Slim::getInstance();
		$catExist = \racoin\common\model\Categorie::find($cat_id);
		if (!is_object($catExist)) {
			echo "Categorie inexistante";
			$app->response->setStatus(400);
		}
		else{
			$annonce = new \racoin\common\model\Annonce();
			$annonce->titre = $titre;
			$annonce->descriptif = $descriptif;
			$annonce->code_postal = $code_postal;
			$annonce->prix = $prix;
			$annonce->nom_a = $nom_a;
			$annonce->cat_id = $cat_id;
			$annonce->dep_id = $dep_id;
			$annonce->passwd = $passwd;
			$annonce->status = '0';
			$annonce->save();

			$lastInsert = $annonce->id;

			$derniereAnnonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','nom_a','mail_a','tel_a','cat_id','dep_id')->where('id','=',$lastInsert)->first();
			if (!is_object($derniereAnnonce)) {
				echo "Mauvais parametre";
				$app->response->setStatus(400);
			}
			elseif (is_object($derniereAnnonce)) {
				$ann = $derniereAnnonce->toArray();
				$res=[
				'annonce'=> $ann,
				'links'=>[ 
					'categorie' => ['href'=> $app->urlFor('ann2cat', ['id'=>$lastInsert])],
					'annonceur' => ['href'=> $app->urlFor('ann2annonceurs', ['id'=>$derniereAnnonce->nom_a])],
					'department' => ['href'=> $app->urlFor('ann2dep', ['id'=>$derniereAnnonce->code_postal])],
					]
				];
				echo json_encode($res, JSON_FORCE_OBJECT);
				$app->response->setStatus(201);
			}

		}
		$app->response->headers->set('Content-Type', 'application/json');
		//http://localhost/API/api-bonracoin/api/index.php/ajouterAnnonce/test/test/57950/15/test/test/1/54

	}

public function ajoutCoordoneeAnnnceur($id_annonce,$prenom_a,$mail_a,$tel_a){
		$app = \Slim\Slim::getInstance();
		$annonceExist = \racoin\common\model\Annonce::find($id_annonce);
		if (!is_object($annonceExist)) {
			echo "Annonce inexistante";
			$app->response->setStatus(400);
		}
		else{
			$annonceExist->prenom_a = $prenom_a;
			$annonceExist->mail_a = $mail_a;
			$annonceExist->tel_a = $tel_a;
			$annonceExist->save();

			$derniereModif = \racoin\common\model\Annonce::select('nom_a','prenom_a','mail_a','tel_a')->where('id','=',$id_annonce)->get();
			if (!is_object($derniereModif)) {
				echo "Mauvais parametre";
				$app->response->setStatus(400);
			}
			elseif (is_object($derniereModif)) {
				$annonceur = $derniereModif->toArray();
				$res=[
				'annonceur'=> $annonceur,
				];
				echo json_encode($res, JSON_FORCE_OBJECT);
				$app->response->setStatus(201);
			}

		}
		$app->response->headers->set('Content-Type', 'application/json');
		//http://localhost/API/api-bonracoin/api/index.php/ajouterCoordonneeAnnonceur/5054/paul/azerty@azerty.com/147852369

	}


public function etatAnnonce($id,$etat){

		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::find($id);
		if (!is_object($annonce)) {
			echo "Annonce inexistante";
			$app->response->setStatus(400);
		}
		else{
			$annonce->status = $etat;
			$annonce->save();

			$derniereModif = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','dep_id','cat_id','status')->where('id','=',$id)->first();
			if (!is_object($derniereModif)) {
				echo "Mauvais parametre";
				$app->response->setStatus(400);
			}
			elseif (is_object($derniereModif)) {
				$ann = $derniereModif->toArray();
				$res=[
				'annonce'=> $ann,
				];
				echo json_encode($res, JSON_FORCE_OBJECT);
				$app->response->setStatus(201);
			}

		}
		$app->response->headers->set('Content-Type', 'application/json');

	}



}