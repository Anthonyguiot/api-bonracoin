<?php
namespace racoin\backend\controller ;

class AdminController
{
	public function requestcredentials($id,$password){
		$app = \Slim\Slim::getInstance();
		$verif = \racoin\common\model\Admin::select('identifiant','password')->where('identifiant','=',$id)->get();
		if (count($verif)==0) {
			echo "Login inexistant";
		}
		else{
		if (!empty($verif[0])) {
			$hash= $verif[0]->password;
			if (password_verify($password, $hash)) {
				$_SESSION['user']=$id;
				$app->redirect($app->urlFor('annoncenonvalide'));
			}
			else {
				$app->redirect($app->urlFor('fail'));
			}
		}

}
	}
}