<?php
namespace racoin\backend\controller ;

class AnnonceController
{
	public function AnnoncesNonValide(){

		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::select('id','titre','prix','nom_a','created_at')->where('status','=',0)->orderBy('created_at', 'ASC')->get();
		if (!is_object($annonce)) {
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
		else{
			$ann = $annonce->toArray();
			return $ann;
		}

	}

	public function getAnnoncebyId($id){
		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::select('id','titre','descriptif','ville','code_postal','prix','cat_id','dep_id')->where('id','=',$id)->first();
			if (!is_object($annonce)) {
				echo "Mauvais parametre";
				$app->response->setStatus(400);
			}
			elseif (is_object($annonce)) {
				$ann = $annonce->toArray();
				return $ann;
			}
	}

	public function AnnoncesCoordonneesAnnonceur($id){
		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::select('nom_a','prenom_a','mail_a','tel_a')->where('id','=',$id)->get();
		if (!is_object($annonce)) {
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
		elseif (is_object($annonce)) {
			$ann = $annonce->toArray();
			return $ann;
		}
	}

	public function etatAnnonce($id,$etat,$rootUri){

		$app = \Slim\Slim::getInstance();
		$annonce = \racoin\common\model\Annonce::find($id);
		if (!is_object($annonce)) {
			echo "Annonce inexistante";
			$app->response->setStatus(400);
		}
		else{
			$annonce->status = $etat;
			$annonce->save();

			$app->redirect($rootUri.'/annonceNonValide');
		}
	}

	public function ajoutAnnonce($titre,$descriptif,$code_postal,$prix,$nom_a,$passwd,$cat_id,$dep_id,$rootUri){
		$app = \Slim\Slim::getInstance();
		$catExist = \racoin\common\model\Categorie::find($cat_id);
		if (!is_object($catExist)) {
			echo "Categorie inexistante";
			$app->response->setStatus(400);
		}
		else{
			$annonce = new \racoin\common\model\Annonce();
			$annonce->titre = $titre;
			$annonce->descriptif = $descriptif;
			$annonce->code_postal = $code_postal;
			$annonce->prix = $prix;
			$annonce->nom_a = $nom_a;
			$annonce->cat_id = $cat_id;
			$annonce->dep_id = $dep_id;
			$annonce->passwd = $passwd;
			$annonce->status = '0';
			$annonce->save();

			$app->redirect($rootUri.'/annonceNonValide');
		}

	}
}