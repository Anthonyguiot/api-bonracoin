<?php
require_once "../vendor/autoload.php";


\app\App::EloConfigure('../conf/config.ini');


$app = new \Slim\Slim();

function apikey(){
$app = \Slim\Slim::getInstance();
$apikey  = $app->request->get('apikey');
$key=\racoin\common\model\Api::select('key','count')->where('key','=',$apikey)->get();
	if (isset($key[0])){
		$app->response->setStatus(201);
		$count=$key[0]->count;
		$key=\racoin\common\model\Api::select('key','count')->where('key','=',$apikey)->update(['count'=>$count+=1]);
	}
	else{
		$app->response->setStatus(403);
		echo json_encode("Cle API non valide");
		$app->stop();
	}
}
$app->get('/' , function() {
	
});


// QUESTION 1
$app->get('/categories/:id','apikey', function($id) use ($app) {
	$c = new \racoin\api\controller\CategorieController();
	$c->typeCategorie($id);
})->name("catByid");

// QUESTION 2
$app->get('/categories','apikey', function() use ($app) {
	$c = new \racoin\api\controller\CategorieController();
	$c->AllCategorie();
});
//question 3
$app->get('/annonces/:id','apikey', function($id) use($app){
	$c = new \racoin\api\controller\AnnonceController();
	//echo $app->response->getStatus();
	$c->getAnnoncebyId($id);
})->name('list2ann');

//question4
$app->get('/annonces','apikey', function() use($app){
	$c = new \racoin\api\controller\AnnonceController();
	$take   = $app->request->get('take');
	$offset = $app->request->get('offset');
	$min    = $app->request->get('min');
	$max    = $app->request->get('max');
	$c->getListAnnonces($offset, $take, $min,$max);
})->name('listann');

// QUESTION 5
$app->get('/annonces/categorie/:id','apikey', function($id) use ($app){
	$app->contentType('application/json');
	$min = $app->request->get('min');
	$max = $app->request->get('max');
	$c = new \racoin\api\controller\AnnonceController();
	$c->AnnoncesByCategorie($id,$min,$max);
})->name("annBycat");

// QUESTION 6
$app->get('/annonces/:id/categories','apikey', function($id) use ($app){
	$c = new \racoin\api\controller\CategorieController();
	$c->CategorieOfAnnonce($id);
})->name('ann2cat');


//QUESTION 104
$app->get('/annonce/nonValide','apikey', function() use ($app) {
	$c = new \racoin\api\controller\AnnonceController();
	$c->AnnoncesNonValide();
});

// QUESTION 106
$app->get('/annonces/:id/valider','apikey', function($id) use ($app) {
	$c = new \racoin\api\controller\AnnonceController();
	$c->AnnoncesValider($id);
});

// QUESTION 22
$app->delete('/supprimer','apikey', function() use ($app){
	$c = new \racoin\api\controller\AnnonceController();
	$id = '5058';
	$mdp = 'azerty';
	$c->AnnoncesSuppr($id,$mdp);
});

// QUESTION 11
$app->post('/categorie/:id_cat/ajouterAnnonce','apikey', function($id_cat) use ($app){
	$c = new \racoin\api\controller\AnnonceController();
	$titre = 'Annonce test categorie';
	$descriptif = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod';
	$code_postal = '54520';
	$prix = '50';
	$nom_a = 'Toto';
	$mdp = 'azerty';
	$passwd = password_hash($mdp, PASSWORD_DEFAULT);
	$cat_id = $id_cat;
	$dep_id = '54';
	$c->ajoutAnnonce($titre,$descriptif,$code_postal,$prix,$nom_a,$passwd,$cat_id,$dep_id);
});

// QUESTION 12
$app->post('/ajouterAnnonce','apikey', function() use ($app){
	$c = new \racoin\api\controller\AnnonceController();
	$titre = 'Annonce test';
	$descriptif = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod';
	$code_postal = '54520';
	$prix = '50';
	$nom_a = 'Toto';
	$mdp = 'azerty';
	$passwd = password_hash($mdp, PASSWORD_DEFAULT);
	$cat_id = '1';
	$dep_id = '54';
	$c->ajoutAnnonce($titre,$descriptif,$code_postal,$prix,$nom_a,$passwd,$cat_id,$dep_id);
});

// QUESTION 20
$app->put('/ajouterCoordonneeAnnonceur','apikey', function() use ($app){
	$c = new \racoin\api\controller\AnnonceController();
	$id_annonce = '5059';
	$prenom_a = 'dupont';
	$mail_a = 'azerty@azerty.fr';
	$tel_a = '0123456789';
	$c->ajoutCoordoneeAnnnceur($id_annonce,$prenom_a,$mail_a,$tel_a);
});

// QUESTION 21
$app->get('/annonce/:id/etat/:etat','apikey', function($id,$etat) use ($app){
	$c = new \racoin\api\controller\AnnonceController();
	$c->etatAnnonce($id,$etat);
});

//QUESTION 19
$app->get('/annonceurs/:id/','apikey', function($id) use($app){
	$c = new \racoin\api\controller\AnnonceController();
	$c->AnnoncesCoordonneesAnnonceur($id);
})->name('ann2annonceurs');

$app->notFound(function () use ($app) {
    echo "Route introuvable";
    $app->response->setStatus(404);
});

$app->run();

